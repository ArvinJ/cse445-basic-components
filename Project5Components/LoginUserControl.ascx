﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginUserControl.ascx.cs" Inherits="Project5Components.LoginUserControl" %>

<div>
    <div>
        <asp:Label ID="Label1" runat="server" Text="Username:"></asp:Label>
        <asp:TextBox ID="usernameBox" runat="server"></asp:TextBox>
    </div>
    <div>
        <asp:Label ID="Label2" runat="server" Text="Password:"></asp:Label>
        <asp:TextBox ID="passwordBox" runat="server"></asp:TextBox>
    </div>

    <asp:Button ID="LoginButton" runat="server" Text="Login" OnClick="LoginButton_Click" />
    <asp:Button ID="RegisterButton" runat="server" Text="Register" OnClick="RegisterButton_Click" />

    <div>
        <asp:Label ID="result" runat="server" Text="Results here"></asp:Label>
    </div>

    <script>
        function triggerRedirection() {
            console.log("redirection event triggered NOW");
            var event = new Event("redirection");
            document.dispatchEvent(event);
        }
    </script>

</div>
