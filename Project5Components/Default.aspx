﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Project5Components._Default" %>
<%@ Register TagPrefix="login" TagName="log" Src="~/LoginUserControl.ascx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <main>
        <login:log runat="server" />

        <asp:Label ID="Label1" runat="server" Text="Once you are registered and logged in, you can view the service"></asp:Label>
        <asp:Button ID="Button1" style="display: none" runat="server" Text="Let's go!" OnClick="Button1_Click" />
    </main>

    <script>
        document.addEventListener("redirection", function () {
            var button = document.getElementById('<%= Button1.ClientID %>');
            if (button) {
                button.style.display = "block";
            } else {
                console.error("Button not found error");
            }
            document.getElementById("Button1").style.display = "block";
        });
    </script>

</asp:Content>
