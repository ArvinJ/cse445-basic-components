﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Project5Components
{
    public partial class LoginUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            string user = usernameBox.Text;
            string password = passwordBox.Text;

            if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(password))
            {
                result.Text = "Please enter everything";
            } else
            {
                if(Valid(user, password))
                {
                    result.Text = "You're logged in!";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "TriggerRedirection", "triggerRedirection();", true);
                } else
                {
                    result.Text = "You're not registered or you entered in something wrong. Consider Registering";
                }
            }
        }

        private bool Valid(string user, string password)
        {
            string filepath = HttpRuntime.AppDomainAppPath + @"\App_Data\Staff.xml";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filepath);

            XmlElement root = xmlDoc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if (node["username"].InnerText == user & node["password"].InnerText == password) 
                {
                    return true;
                }
            }

            return false;
        }

        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            string user = usernameBox.Text;
            string password = passwordBox.Text;

            if(string.IsNullOrEmpty(user) || string.IsNullOrEmpty(password)) 
            {
                result.Text = "Please enter everyting";
            } else
            {
                if(Valid(user, password))
                {
                    result.Text = "You're already registered. Please log in";
                } else
                {
                    string filepath = HttpRuntime.AppDomainAppPath + @"\App_Data\Staff.xml";
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(filepath);

                    XmlElement root = xmlDoc.DocumentElement;

                    // create user, then username, then password

                    // creating user
                    XmlElement newUser = xmlDoc.CreateElement("user", root.NamespaceURI);
                    root.AppendChild(newUser);

                    // creating username
                    XmlElement newUsername = xmlDoc.CreateElement("username", root.NamespaceURI);
                    newUsername.InnerText = user;
                    newUser.AppendChild(newUsername);

                    // creating password
                    XmlElement newPassword = xmlDoc.CreateElement("password", root.NamespaceURI);
                    newPassword.InnerText = password;
                    newUser.AppendChild(newPassword);

                    xmlDoc.Save(filepath);

                    result.Text = "You are now registered. Please log in";
                }
            }
        }
    }
}